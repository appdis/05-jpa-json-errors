package ec.edu.ups.appdis.pedidos.dao;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;

import ec.edu.ups.appdis.pedidos.modelo.Cliente;
import ec.edu.ups.appdis.pedidos.modelo.Direccion;

@Stateless
public class ClienteDAO {

	@Inject 
	private EntityManager em;
	
	public void insert(Cliente cli) {
		em.persist(cli);
	}
	
	public Cliente read(int id) {
		try {
			return em.find(Cliente.class, id);
		}catch(Exception e) {
			return null;
		}
		
	}
	
}

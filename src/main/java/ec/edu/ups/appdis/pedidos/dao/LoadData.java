package ec.edu.ups.appdis.pedidos.dao;

import java.util.Date;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.inject.Inject;

import ec.edu.ups.appdis.pedidos.modelo.Cliente;
import ec.edu.ups.appdis.pedidos.modelo.Direccion;
import ec.edu.ups.appdis.pedidos.modelo.Empleado;
import ec.edu.ups.appdis.pedidos.modelo.Ordene;

@Singleton
@Startup
public class LoadData {
	
	@Inject
	private OrdenesDAO dao;
	
	@Inject
	private ClienteDAO daoCli;
	
	@Inject
	private EmpleadoDAO daoEMp;
	
    @PostConstruct
    private void postConstruct() {  
    	try{
    		if(daoCli.read(1)==null) {
    			this.saveCliente();
        		this.saveEmpleado();
    		}
    	}catch(Exception e) {
    		
    	}
    	
    	//this.saveOrden(100);
    }

    @PreDestroy
    private void preDestroy() { /* ... */ }
    
    public String saveCliente() {
		Cliente cliente = new Cliente();
		cliente.setClienteid(1);
		cliente.setCedulaRuc("0103709911");
		cliente.setCelular("0984864034");
		cliente.setEmail("ctimbi@gmail.com");
		cliente.setDireccioncli("Monay");
		cliente.setNombrecia("JA");
		cliente.setNombrecontacto("Juan");
		daoCli.insert(cliente);
		
		return null;
	}
	
	public String saveEmpleado() {
		Empleado emp = new Empleado();
		emp.setEmpleadoid(10);
		emp.setApellido("Perez");
		emp.setNombre("Juan");
		emp.setExtension(152);
		emp.setFechaNac(new Date());		
		daoEMp.insert(emp);
		
		return null;
	}
	
	public String saveOrden(int id) {
		
		Ordene orden = new Ordene();
		orden.setOrdenid(id);
		orden.setDescuento(5);
		orden.setFechaorden(new Date());
		
		Direccion dir1 = new Direccion();
		dir1.setCodigo(1*id);
		dir1.setCiudad("Guayaquil");
		dir1.setDireccion("Sur");
		
		
		orden.addDireccion(dir1);
		
		Direccion dir2 = new Direccion();
		dir2.setCodigo(2*id);
		dir2.setCiudad("Quito");
		dir2.setDireccion("Sangolqui");
		
		orden.addDireccion(dir2);
		
		Cliente cliente = new Cliente();
		cliente.setClienteid(1);
		
		orden.setCliente(cliente);
		
		Empleado emp = new Empleado();
		emp.setEmpleadoid(10);
		
		orden.setEmpleado(emp);
		
		dao.insert(orden);
		
		return null;
	}
}
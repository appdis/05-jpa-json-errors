package ec.edu.ups.appdis.pedidos.service;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;

import ec.edu.ups.appdis.pedidos.dao.CategoriaDAO;
import ec.edu.ups.appdis.pedidos.dao.OrdenesDAO;
import ec.edu.ups.appdis.pedidos.modelo.Categoria;
import ec.edu.ups.appdis.pedidos.modelo.CategoriaTemp;
import ec.edu.ups.appdis.pedidos.modelo.Ordene;
  
@Path("/ordenes")
public class OrdeneService {
	
	@Inject
	private OrdenesDAO dao;
	
	@Inject
	private CategoriaDAO daoCat;
	
	@GET
	@Path("/orden")
	@Produces("application/json")
	public Ordene getOrden(@QueryParam("id") int id) {
		
		return dao.read(id);
	}
	
	
	@GET
	@Path("/ordenes")
	@Produces("application/json")
	public List<Ordene> getOrdenes() {
		List<Ordene> ordenes = dao.getList4();		
		return ordenes;
	}
	
	
	
	
	@GET
	@Path("/categorias2")
	@Produces("application/json")
	public List<CategoriaTemp> getCategorias() {
		List<Categoria> categorias = daoCat.getCategorias();
		List<CategoriaTemp> cats = new ArrayList<>();
		for(Categoria cat : categorias) {
			CategoriaTemp temp = new CategoriaTemp();
			temp.setCategoriaid(cat.getCategoriaid());
			temp.setNombrecat(cat.getNombrecat());
			cats.add(temp);
		}
		return cats;
	}
	
}

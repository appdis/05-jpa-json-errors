package ec.edu.ups.appdis.pedidos.dao;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;

import ec.edu.ups.appdis.pedidos.modelo.Cliente;
import ec.edu.ups.appdis.pedidos.modelo.Direccion;
import ec.edu.ups.appdis.pedidos.modelo.Empleado;

@Stateless
public class EmpleadoDAO {

	@Inject 
	private EntityManager em;
	
	public void insert(Empleado cli) {
		em.persist(cli);
	}
	
}
